﻿///////////////////////////////////////////////////////////////////////////////////
　　//////////////////////////// 　　文字挿入　　　////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

// ブランド略称
// NS: 	str / cym
// MSK: cym / frt
// ONC: alm / ram => lilly
// ONC: abe
// DMG: tlc / gly / hlg /hmt / trz / jad / baq / lum => lilly
// AI: tal / olm
var PRODUCT = 'ram';

// イベントID
// yymmdd + ブランド略称
var EVENTID = '2204ram32';

// GAタグ用prefix / AUTO
var PREFIX = 'veeva_vod' + EVENTID;

//「いいえ」のリンク先
var NOLINK = 'https://www.locj.jp/';

//「電子添文」のリンク先　（未入力の場合は、電子添文ボタンが非表示になる）
var DOCUMENT = 'https://web-cache.stream.ne.jp/web/live/lilly/ppi_lilly/onc/cyramza/';

//  演題
var TITLE01 = 'サイラムザ+エルロチニブ療法で治療をはじめられる方へ';

//  副題
var TITLE02 = '';

//  EQ MID - Normal
var MOVIEVOD = '816';

//  EQ MID - High Speed
var HSMOVIEVOD = '816';


///////////////////////////////////////////////////////////////////////////////////
/////
///// UA判別
/////

var getDevice = (function(){
    var ua = navigator.userAgent;
    if(ua.indexOf('iPhone') > 0 || ua.indexOf('iPod') > 0 || ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0){
        return 'sp';
    }else if(ua.indexOf('iPad') > 0 || ua.indexOf('Android') > 0){
        return 'tab';
    }else{
        return 'other';
    }
})();


///////////////////////////////////////////////////////////////////////////////////
/////
///// 自動生成
/////

// ページタイトル <title></title>
// document.title = TITLE01 + TITLE02;
if(document.URL.match('movie')) {
  if (TITLE01.indexOf('<br>') !== -1 || TITLE02.indexOf('<br>') !== -1) {
    DOCTITLE = TITLE01.replace("<br>", "");
    if (TITLE02 != ''){
      DOCTITLE += ' ' + TITLE02.replace("<br>", "");
    }
  } else {
    DOCTITLE = TITLE01 + ' ' + TITLE02;
  }
  document.title = DOCTITLE;
}

// ロゴパス / AUTO
if (PRODUCT == 'alm' || PRODUCT == 'ram' || PRODUCT == 'tlc' || PRODUCT == 'gly' || PRODUCT == 'hlg' || PRODUCT == 'hmt' || PRODUCT == 'trz' || PRODUCT == 'jad' || PRODUCT == 'baq' || PRODUCT == 'lum'){
  var LOGOPC = '<img src="../../../common/images/logo_lilly_small.png" alt="">';
} else {
  var LOGOPC = '<img src="../../../common/images/logo_' + PRODUCT + '.png" alt="">';
}

// フッター共通
var FOOTERLOGO = '<img src="../../../common/images/logo_lilly.png" alt="Lilly"/>';
//var FLASHBANNER = '<a href="http://get.adobe.com/jp/flashplayer/" target="_blank" onclick="ga(\'send\', \'event\', \'Outbound\', \'Click\', \'http://get.adobe.com/jp/flashplayer/_' + '_' + EVENTID + '\');" class="tacosLogEvent" data-eventcd="flash"><img src="../../../common/images/flash.png" alt=""/></a>';
var FLASHBANNER = '';
//var FLASHBANNERTXT = '<img src="../../../common/images/flash_text.png" alt=""/>';
var FLASHBANNERTXT = '';
//var NOTICETXT = '視聴に関する技術的なお問合せ<br>0120-000-xxx（視聴サポートデスク）<br>対応時間　平日xx:00～xx:00まで<br>（ライブ配信がある場合はライブ配信終了まで［最長xx:00まで］）'
var COPYRIGHT = 'Copyright © 2023 Eli Lilly Japan K.K. All rights reserved';

// GAイベントラベル
var EVENTLABELNOLINK = 'ga(\'send\', \'event\', \'Outbound\', \'Click\', \'' + NOLINK + '_' + '_' + EVENTID + '\');';
var EVENTLABELPRECAUTIONS = 'ga(\'send\', \'event\', \'Inbound\', \'Click\', \'attention/index.html_' + '_' + EVENTID + '\');';

$(function() {
	$('.nolink').attr('href', NOLINK);
	$('.blandLogo02').html(LOGOPC);
	$('.text-01').html(TITLE01);
	$('.text-02').html(TITLE02);
  $('.eqPlayer').attr('data-normal-speed-mid', MOVIEVOD);

	// 倍速再生が必要な場合のみ表示
	$('.speedChg').hide();
	if (HSMOVIEVOD != ''){
		$('.speedChg').show();
    $('.eqPlayer').attr('data-high-speed-mid', HSMOVIEVOD);
	};

	// 電子添文がある場合のみ表示
	$('.document').hide();
	if (DOCUMENT != ''){
		$('.document').html('<a href="' + DOCUMENT + '" target="_blank" class="tacosLogEvent" data-eventcd="document" onClick="ga(\'send\', \'event\', \'Outbound\', \'Click\', \'' + DOCUMENT + '_' + '_' + EVENTID + '\');">電子添文情報を見る</a>');
		$('.document').show();
	};

  // DMG領域のみフッターロゴの表示非表示
  // ONC: alm / ram
  // DMG: tlc / gly / hlg /hmt / trz / jad
  if (PRODUCT == 'alm' || PRODUCT == 'ram' || PRODUCT == 'tlc' || PRODUCT == 'gly' || PRODUCT == 'hlg' || PRODUCT == 'hmt' || PRODUCT == 'trz' || PRODUCT == 'jad' || PRODUCT == 'baq' || PRODUCT == 'lum'){
    $('.ft_02 p.logo').hide();
  }

  // フッター共通
  if(document.URL.match('movie')) {
    $('.logo').html(FOOTERLOGO);
  }
  $('.flaBtn').html(FLASHBANNER);
  $('.flaTxt').html(FLASHBANNERTXT);
  $('.copy').html(COPYRIGHT);

  // GAイベントラベル
  $('.nolink').attr('onClick', EVENTLABELNOLINK);
  $('.precautions').attr('onClick', EVENTLABELPRECAUTIONS);

});


// Google Analytics
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-69864277-7', 'auto'); //IDを更新
ga('send', 'pageview');



///////////////////////////////////////////////////////////////////////////////////
/////
///// viewport.js
/////

(function() {

var isThis = new function() {
	this.ua = navigator.userAgent;

	this.iPhone = this.ua.indexOf("iPhone") != -1;
	this.iPad = this.ua.indexOf("iPad") != -1;
	this.iOS = this.iPhone || this.iPad;

	this.Android = this.ua.indexOf("Android") != -1;
	this.Android2 = this.ua.indexOf("Android 2") != -1;
	this.Android4 = this.ua.indexOf("Android 4") != -1;
	this.AndroidSmartPhone = this.Android && this.ua.indexOf("Mobile") != -1 && this.ua.indexOf("A1_07") == -1 && this.ua.indexOf("SC-01C") == -1;
	this.AndroidTablet = this.Android && (this.ua.indexOf("Mobile") == -1 || this.ua.indexOf("A1_07") != -1 || this.ua.indexOf("SC-01C") != -1);

	this.sp = this.iPhone || this.AndroidSmartPhone;
	this.tablet = this.iPad || this.AndroidTablet;
};

var isThisPc = new function() {
	this.ua = navigator.userAgent;

	this.windows = this.ua.indexOf("Windows NT") != -1;
	this.mac = this.ua.indexOf("Macintosh") != -1;

	this.ie11 = this.ua.indexOf("Trident/7") != -1;
	this.edge = this.ua.indexOf("Edge") != -1;
	this.firefox = this.ua.indexOf("Firefox") != -1;
	this.chrome = this.ua.indexOf("Chrome") != -1 && this.ua.indexOf("Edge") == -1;
	this.safari = this.ua.indexOf("Safari") != -1 && this.ua.indexOf("Chrome") == -1;
};

if(document.URL.match('movie')) {
  if (isThis.sp) {
  	document.write('<meta name="viewport" content="width=device-width,initial-scale=1">');
  	document.write('<link rel="stylesheet" type="text/css" href="../../../common/css/style_sp.css">');
  } else if (isThis.tablet) {
  	document.write('<meta name="viewport" content="width=device-width,initial-scale=1">');
  	document.write('<link rel="stylesheet" type="text/css" href="../../../common/css/style_tablet.css">');
  } else {
  	document.write('<meta name="viewport" content="width=1000">');
  	document.write('<link rel="stylesheet" type="text/css" href="../../../common/css/style_pc.css">');
  }
  // ONC: alm / ram
  // DMG: tlc / gly / hlg /hmt / trz / jad / baq / lum
  if (PRODUCT == 'alm' || PRODUCT == 'ram' || PRODUCT == 'tlc' || PRODUCT == 'gly' || PRODUCT == 'hlg' || PRODUCT == 'hmt' || PRODUCT == 'trz' || PRODUCT == 'jad' || PRODUCT == 'baq' || PRODUCT == 'lum'){
    document.write('<link rel="stylesheet" type="text/css" href="../../../common/brand/lilly/css/color.css">');
  } else {
    document.write('<link rel="stylesheet" type="text/css" href="../../../common/brand/' + PRODUCT + '/css/color.css">');
  }

  window.isThis = isThis;
  window.isThisPc = isThisPc;
}



})();

if(document.URL.match('movie')) {

///////////////////////////////////////////////////////////////////////////////////
/////
///// textfit.js
/////

(function() {
  var isThis = new function() {
		this.ua = navigator.userAgent;

		this.iPhone = this.ua.indexOf("iPhone") != -1;
		this.iPad = this.ua.indexOf("iPad") != -1;
		this.iOS = this.iPhone || this.iPad;

		this.Android = this.ua.indexOf("Android") != -1;
		this.Android2 = this.ua.indexOf("Android 2") != -1;
		this.Android4 = this.ua.indexOf("Android 4") != -1;
		this.AndroidSmartPhone = this.Android && this.ua.indexOf("Mobile") != -1 && this.ua.indexOf("A1_07") == -1 && this.ua.indexOf("SC-01C") == -1;
		this.AndroidTablet = this.Android && (this.ua.indexOf("Mobile") == -1 || this.ua.indexOf("A1_07") != -1 || this.ua.indexOf("SC-01C") != -1);

		this.sp = this.iPhone || this.AndroidSmartPhone;
		this.tablet = this.iPad || this.AndroidTablet;
	};

	$(document).ready(function(){
		// 日本語で幅計算が狂うため、計算する前に文字数を調整する値を設定
		var adjust_val = 0.7;
		// JSに渡す初期値をオブジェクトとして設定
		var values = {
			line01: {
				maxFont: 26,
				minFont: 15,
			},
			line02: {
				maxFont: 15,
				minFont: 12,
			}
		};
		// 端末とラインそれぞれの最大値、最小値を設定
		var options = {
			pc:{
				line01:{
					maxFont: 26, // PC メインタイトル最大サイズ
					minFont: 20,　// PC メインタイトル最小サイズ
				},
				line02:{
					maxFont: 26, // PC サブタイトル最大サイズ
					minFont: 20, // PC サブタイトル最小サイズ
				}
			},
			tablet:{
				line01:{
					maxFont: 30, // タブレット メインタイトル最大サイズ
					minFont: 14, // タブレット メインタイトル最小サイズ
				},
				line02:{
					maxFont: 20, // タブレット サブタイトル最大サイズ
					minFont: 14, // タブレット サブタイトル最小サイズ
				}
			},
			sp: {
				line01:{
					maxFont: 20, // スマホ メインタイトル最大サイズ
					minFont: 8, // スマホ メインタイトル最小サイズ
				},
				line02:{
					maxFont: 13, // スマホ サブタイトル最大サイズ
					minFont: 8, // スマホ サブタイトル最小サイズ
				}
			},
		};

		if (isThis.sp) { // SP
			values = {
				line01: {
					maxFont: options.sp.line01.maxFont,
					minFont: options.sp.line01.minFont,
				},
				line02: {
					maxFont: options.sp.line02.maxFont,
					minFont: options.sp.line02.minFont,
				}
			};
		} else if (isThis.tablet) { //Tablet
			values = {
				line01: {
					maxFont: options.tablet.line01.maxFont,
					minFont: options.tablet.line01.minFont,
				},
				line02: {
					maxFont: options.tablet.line02.maxFont,
					minFont: options.tablet.line02.minFont,
				}
			};
		} else { //PC
			values = {
				line01: {
					maxFont: options.pc.line01.maxFont,
					minFont: options.pc.line01.minFont,
				},
				line02: {
					maxFont: options.pc.line02.maxFont,
					minFont: options.pc.line02.minFont,
				}
			};
		}

		// console.log(values);

		// $(window).on('load resize orientationchange' ,function(){

			var $text01 = $('.fit-01');
			var $text02 = $('.fit-02');

			function countLineLength(target) {
				var fullText = target.html();
				var lineText = fullText.replace(/<br(.*)>(.*)/g, '');
				var lineLength = lineText.length + adjust_val;
				// console.log('lineLength' + lineLength);
				return lineLength;
			}
		  $text01.flowtype({
		    maximum: 9999,
		    minimum: 1,
		    maxFont: values.line01.maxFont,
		    minFont: values.line01.minFont,
		    fontRatio: countLineLength($text01)
		  });

		  $text02.flowtype({
		    maximum: 9999,
		    minimum: 1,
		    maxFont: values.line02.maxFont,
		    minFont: values.line02.minFont,
		    fontRatio: countLineLength($text02)
		  });
		});

	// });
})();



///////////////////////////////////////////////////////////////////////////////////
/////
///// index.js
/////

(function() {
	var player;

	var mediaInfo = {
		speedMode: 'normal',
		currentTime: 0,
		normalSpeedModeText: '通常の速度に戻す<br>（1.0倍）',
		highSpeedModeText: '倍速再生で視聴する<br>（1.3倍）',
		highSpeedRate: 1.3,
		rewindTime: 1
	};

	$(function() {
		$('#speedChange').html(mediaInfo.highSpeedModeText).addClass('normal');;
		mediaInfo.mid = $('#eqPlayer').attr('data-normal-speed-mid');
		mediaInfo.hsMid = $('#eqPlayer').attr('data-high-speed-mid');

		setPlayer(mediaInfo.mid, 0);
		setSpeedChangeClickEvent();
	});

	function setPlayer(mid, startTime, autoplay) {
		var height = $('.playerBox .player').innerHeight();
		var width = $('.playerBox .player').innerWidth();
		var ip = autoplay ? "on" : "off"

    player = jstream_t3.PlayerFactoryIF.create({
			b: "eqb673ulpm.eq.webcdn.stream.ne.jp/www50/eqb673ulpm/jmc_pub/jmc_swf/player/",
			c: "MjA1Mg==",
			m: mid,
			t: startTime,
			s: {
				dq: "0",
				el: "off",
				rp: 'fit',
				sn: "",
				hp: height,
				wp: width,
				tg: "off",
				ti: "off",
				ip: ip
			}
		}, "eqPlayer");

		EQP.control(mid, player);
	}

	function setSpeedChangeClickEvent() {
		$('#speedChange').on('click', function() {
			var playerState = player.accessor.state;
			if (playerState !== 'landing' && playerState !== 'playing' &&
				playerState !== 'paused' && playerState !== 'complete') {
				return false;
			}

			mediaInfo.currentState = playerState;

			if (mediaInfo.speedMode === 'normal') {
				mediaInfo.currentTime = player.accessor.getCurrentTime();
				mediaInfo.speedMode = 'highSpeed';
			} else {
				mediaInfo.currentTime = player.accessor.getCurrentTime() * mediaInfo.highSpeedRate;
				mediaInfo.speedMode = 'normal';
			}
			changeSpeed();
			return false;
		});
	}

	function changeSpeed() {
		var mid, text, startTime, className;
		if (mediaInfo.speedMode === 'normal') {
			mid = mediaInfo.mid;
			text = mediaInfo.highSpeedModeText;
			startTime = mediaInfo.currentTime - mediaInfo.rewindTime;
			className = mediaInfo.speedMode;
		} else {
			mid = mediaInfo.hsMid;
			text = mediaInfo.normalSpeedModeText;
			startTime = (mediaInfo.currentTime - mediaInfo.rewindTime) / mediaInfo.highSpeedRate;
			className = mediaInfo.speedMode;
		}

		if (startTime < 0) {
			startTime = 0;
		}

		$('#speedChange').html(text).removeClass().addClass(className);

		var autoplay = mediaInfo.currentState === 'playing' && (isThisPc.windows && (isThisPc.edge || isThisPc.firefox || isThisPc.ie11) ? true : false);

		setPlayer(mid, startTime, autoplay);
	}
})();

}

