// import $ from 'jquery';
import querystring from 'querystring';


/**
 * 配列と配列の要素が一致するか判別
 */
export const isMatchArray = (a, b) => {
	if (Array.isArray(a) && Array.isArray(b) && a.sort().toString() === b.sort().toString()) {
		return true;
	}
	return false;
}

/**
 * 配列内に値が存在するか判別
 */
export const inArray = (array, value) => {
	if (Array.isArray(array) && array.indexOf(value) >= 0) {
		return true;
	}
	return false;
}

/**
 * イベント名取得
 */
export const getEventName = () => {
	return window.EVENTID ? window.EVENTID : window.location.pathname.split("/event/")[1].split("/").filter(Boolean)[0];
}

/**
 * URLクエリの取得
 */
export const getUrlQuery = () => {
	return querystring.parse(window.location.search.substring(1));
}

/**
 * デバッグモード判定
 */
export const isDebugMode = () => {
	const urlparse = querystring.parse(window.location.search.substring(1))

	return urlparse && urlparse.debug === "on";
}

/**
 * 相対URLを絶対URLに変換
 */
export const getAbsUrl = (relativePath) => {
	const a = document.createElement('a');
	a.href = relativePath;
	return a.href;
}

/**
 * EQのAPIリファレンスのパース
 */
export const parseApiKeywords = (apiKeywords) => {
	let obj = {};

	if (apiKeywords) {
		obj = apiKeywords.split(";").reduce((result, query) => {
			let [key, value] = query.split("=");

			if (!result[key])  {
			// keyがtagのときは、単体のときでも配列に入れる
				if(["tag","list",].includes(key)){
					result[key] = value.split();
				}else{
					result[key] = value;
				}
			} else {
				if (Array.isArray(result[key])) {
					result[key].push(value);
				} else {
					result[key] = [result[key], value];
				}
			}

			return result;
		}, {});
	}

	return obj;
}